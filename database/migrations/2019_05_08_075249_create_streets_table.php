<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreetsTable extends Migration
{

    public function up()
    {
        Schema::create('streets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_jalan');
            $table->string('nama_daerah');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('streets');
    }
}
