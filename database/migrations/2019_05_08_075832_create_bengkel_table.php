<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBengkelTable extends Migration
{

    public function up()
    {
        Schema::create('bengkel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_bengkel');
            $table->string('jenis_bengkel');
            $table->string('no_usaha');
            $table->unsignedInteger('street_id');
            $table->foreign('street_id')->references('id')->on('streets')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bengkel');
    }
}
