<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(StreetTableSeeder::class);
        $this->call(BengkelTableSeeder::class);
    }
}
