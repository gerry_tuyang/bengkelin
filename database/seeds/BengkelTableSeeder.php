<?php

use Illuminate\Database\Seeder;

class BengkelTableSeeder extends Seeder
{

    public function run()
    {
        
    	DB::table('bengkel')->insert([
        	[
        		'nama_bengkel'=>'Jaya Abadi', 'jenis_bengkel'=>'Bengkel Motor',
            	'no_usaha'=>'00551', 'street_id'=>'2'
        	],
        	[
        		'nama_bengkel'=>'Tiga Putra', 'jenis_bengkel'=>'Bengkel Motor',
            	'no_usaha'=>'00222', 'street_id'=>'3'
        	],
        	[
        		'nama_bengkel'=>'Jaya Permata', 'jenis_bengkel'=>'Bengkel Mobil',
            	'no_usaha'=>'01210', 'street_id'=>'1'
        	],
        	[
        		'nama_bengkel'=>'Five Fix', 'jenis_bengkel'=>'Bengkel Mobil',
            	'no_usaha'=>'01155', 'street_id'=>'5'
        	],
        	[
        		'nama_bengkel'=>'Mantap Jiwa', 'jenis_bengkel'=>'Bengkel Motor/Mobil',
            	'no_usaha'=>'20223', 'street_id'=>'4'
        	],
        	[
        		'nama_bengkel'=>'Onderdil One', 'jenis_bengkel'=>'Bengkel Motor/Mobil',
            	'no_usaha'=>'44419', 'street_id'=>'3'
        	],
        ]);

    }
}
