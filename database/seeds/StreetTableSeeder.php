<?php

use Illuminate\Database\Seeder;

class StreetTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('streets')->insert([
        	[
        		'nama_jalan'=>'Jl. Perjuangan', 'nama_daerah'=>'Perjuangan',
        	],
        	[
        		'nama_jalan'=>'Jl. Suwandi', 'nama_daerah'=>'Vorvo',
        	],
        	[
        		'nama_jalan'=>'Jl. Pramuka', 'nama_daerah'=>'Pramuka',
        	],
        	[
        		'nama_jalan'=>'Jl. Anggur', 'nama_daerah'=>'Anggur',
        	],
        	[
        		'nama_jalan'=>'Jl. Sentosa', 'nama_daerah'=>'Alaya',
        	],
        ]);
    }
}
