<?php
Route::get('/', function () {
    return view('welcome');
});
Route::get('/contact', function () {
    return view('contact');
});

Auth::routes();

Route::group(['prefix' => 'isAdmin', 'middleware' => ['admin', 'auth']], function(){
	Route::get('/admin', 'AdminController@index')->name('admin.index');
	Route::resource('users', 'UserController');
	Route::resource('bengkel', 'BengkelController');
	Route::resource('streets', 'StreetController');
});

Route::resource('home', 'HomeController');
