<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customers";

    protected $guarded = [];

    public function pembayaran()
    {
    	return $this->hasMany(Pembayaran::class, 'customer_id', 'id');
    }

    public function service()
    {
    	return $this->hasMany(Service::class, 'customer_id', 'id');
    }
}
