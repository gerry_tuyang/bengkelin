<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Street;

class Bengkel extends Model
{
    protected $table = "bengkel";

    protected $guarded = [];

    public function street()
    {
        return $this->belongsTo(Street::class, 'street_id', 'id');
    }

    public function service()
    {
        return $this->belongsToMany('App\Service');
    }
}
