<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;

class Service extends Model
{
    protected $table = "services";

    protected $guarded = [];

    public function customer()
    {
    	return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function bengkel()
    {
    	return $this->belongsToMany('App\Bengkel');
    }
}
