<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{

    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::User()->isAdmin())
        {
            return $next($request);
        }
        return redirect('home.index');
    }
}
