<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function index()
    {
    	$users = User::all();
    	return view('users.index', compact('users'));
    }

    public function show($id)
    {
    	$users = User::findOrFail($id);
    	return view('users.show', compact('users'));
    }

    public function create()
    {
    	$users = User::all();
    	return view('users.create', compact('users'));
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'name'		=> 'required|min:3|max:50',
    		'email'		=> 'required|min:5|max:50',
    		'phone'		=> 'required|max:15',
    		'address'	=> 'required',
            'password'  => 'required|string|min:4|confirmed',
    	]);

    	User::create([
    		'name'		=> $request->name,
    		'email'		=> $request->email,
    		'phone'		=> $request->phone,
    		'address'	=> $request->address,
            'password'  => Hash::make($request['password']),
    	]);

    	return redirect()->route('users.index');
    }

    public function edit($id)
    {
    	$users = User::findOrFail($id);
    	return view('users.edit', compact('users'));
    }

    public function update(Request $request, $id)
    {
    	$users = User::findOrFail($id);
    	$request->validate([
    		'name'		=> 'required',
    		'email'		=> 'required',
    		'phone'		=> 'required',
    		'address'	=> 'required',
            'password'  => 'required|string|min:4|confirmed',
    	]);

    	$users->update([
    		'name'		=> $request->name,
    		'email'		=> $request->email,
    		'phone'		=> $request->phone,
    		'address'	=> $request->address,
            'password'  => Hash::make($request['password']),
    	]);

    	$users->save();
    	return redirect()->route('users.index', ['users'=>$users]);
    }

    public function destroy($id)
    {
    	$users = User::findOrFail($id);
    	$users->delete();
    	return redirect()->back();
    }
}
