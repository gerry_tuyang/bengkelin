<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bengkel;
use App\Street;

class BengkelController extends Controller
{
    public function index()
    {
    	$bengkel = Bengkel::all();
    	return view('bengkel.index', compact('bengkel'));
    }

    public function show($id)
    {
    	$bengkel = Bengkel::findOrFail($id);
    	return view('bengkel.show', compact('bengkel'));
    }

    public function create()
    {
    	$streets = Street::all();
    	return view('bengkel.create', ['streets'=>$streets]);
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'nama_bengkel'	=> 'required',
    		'jenis_bengkel'	=> 'required',
    		'no_usaha'		=> 'required',
    		'street_id'		=> 'required',
    	]);

    	Bengkel::create([
    		'nama_bengkel'	=> $request->nama_bengkel,
    		'jenis_bengkel'	=> $request->jenis_bengkel,
    		'no_usaha'		=> $request->no_usaha,
    		'street_id'		=> $request->street_id,
    	]);

    	return redirect()->route('bengkel.index');
    }

    public function edit($id)
    {
    	$bengkel = Bengkel::findOrFail($id);
    	$streets = Street::all();
    	return view('bengkel.edit', ['bengkel'=>$bengkel, 'streets'=>$streets]);
    }

    public function update(Request $request, $id)
    {
    	$bengkel = Bengkel::findOrFail($id);
    	$request->validate([
    		'nama_bengkel'	=> 'required',
    		'jenis_bengkel'	=> 'required',
    		'no_usaha'		=> 'required',
    		'street_id'		=> 'required',
    	]);

    	$bengkel->update([
    		'nama_bengkel'	=> $request->nama_bengkel,
    		'jenis_bengkel'	=> $request->jenis_bengkel,
    		'no_usaha'		=> $request->no_usaha,
    		'street_id'		=> $request->street_id,
    	]);

    	return redirect()->route('bengkel.index', ['bengkel'=>$bengkel]);
    }

    public function destroy($id)
    {
    	$bengkel = Bengkel::findOrFail($id);
    	$bengkel->delete();
    	return redirect()->back();
    }
}



