<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Street;

class StreetController extends Controller
{
    public function index()
    {
    	$streets = Street::all();
    	return view('streets.index', compact('streets'));
    }

    public function show($id)
    {
        $streets = Street::findOrFail($id);
        return view('streets.show', compact('streets'));
    }

    public function create()
    {
    	$streets = Street::all();
    	return view('streets.create', compact('streets'));
    }

    public function store(Request $request)
    {
    	$request->validate([
            'nama_jalan'    =>'required|min:5|max:300',
            'nama_daerah'   =>'required|min:5|max:200',
        ]);

        Street::create([
            'nama_jalan'    =>$request->nama_jalan,
            'nama_daerah'   =>$request->nama_daerah,
        ]);

        return redirect()->route('streets.index');
    }

    public function edit($id)
    {
        $streets = Street::findOrFail($id);
        return view('streets.edit', ['streets'=>$streets]);
    }

    public function update(Request $request, $id)
    {
        $streets = Street::findOrFail($id);
        $request->validate([
            'nama_jalan'    => 'required',
            'nama_daerah'   => 'required',
        ]);

        $streets->update([
            'nama_jalan'    => $request->nama_jalan,
            'nama_daerah'   => $request->nama_daerah,
        ]);

        $streets->save();
        return redirect()->route('streets.index', ['streets'=>$streets]);
    }

    public function destroy($id)
    {
        $streets = Street::findOrFail($id);
        $streets->delete();
        return redirect()->back();
    }
}