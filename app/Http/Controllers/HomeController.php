<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Bengkel;
use App\User;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$bengkel = Bengkel::all();
        return view('home.index', compact('bengkel'));
    }

    public function show($id)
    {
    	$users = User::findOrFail($id);
    	return view('home.show', compact('users'));
    }

    public function edit($id)
    {
    	$users = User::findOrFail($id);
    	return view('home.edit', compact('users'));
    }

    public function update(Request $request, $id)
    {
    	$users = User::findOrFail($id);
    	$request->validate([
    		'name'		=> 'required',
    		'email'		=> 'required',
    		'phone'		=> 'required',
    		'address'	=> 'required',
            'password'  =>  'required|string|min:4|confirmed',
    	]);

    	$users->update([
    		'name'		=> $request->name,
    		'email'		=> $request->email,
    		'phone'		=> $request->phone,
    		'address'	=> $request->address,
    		'password'	=> Hash::make($request['password']),
    	]);

    	$users->save();
    	return redirect()->route('home.show', ['users'=>$users]);
    }
}
