<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;

class Pembayaran extends Model
{
    protected $table = "pembayaran";

    protected $guarded = [];

    public function customer()
    {
    	return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
