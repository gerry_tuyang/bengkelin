<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bengkel;

class Street extends Model
{
    protected $table = "streets";

    protected $guarded = [];

    public function bengkel()
    {
    	return $this->hasMany(Bengkel::class, 'street_id', 'id');
    }
}
