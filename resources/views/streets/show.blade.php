@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
	<div class="card">
		<div class="card-header bg-success text-light">
			Detail Street
		</div>

		<div class="card-body">

			<div class="row">
				<div class="col-md-2">
					<label for="nama_jalan">Nama Jalan</label>
				</div>
				<div class="col-md-9">
					<input type="text" readonly="" value="{{$streets['nama_jalan']}}" class="form-control">
				</div>
			</div>
		<br>
			<div class="row">
				<div class="col-md-2">
					<label for="nama_daerah">Nama  Daerah</label>
				</div>
				<div  class="col-md-9">
					<input type="text" value="{{$streets['nama_daerah']}}" class="form-control" readonly="">
				</div>
			</div>

		</div>

	</div>
</div>

@endsection