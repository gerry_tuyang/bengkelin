@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
    <div class="card">
        <div class="card-header bg-success text-light">
            Edit Street
        </div>

        <div class="card-body">
            <form class="form-group" action="{{ route('streets.update', $streets['id']) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

                <div class="row">
                    <div class="col-md-2">
                        <label for="nama_jalan">Nama Jalan</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('nama_jalan')? "is-invalid": ""}}" id="nama_jalan" name="nama_jalan" value=" {{ old('nama_jalan') ? old('nama_jalan') : $streets['nama_jalan'] }} ">
                        <div class="invalid-feedback">
                          {{$errors->first('nama_jalan')}}
                        </div>
                    </div>
                </div>

              <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="nama_daerah">Nama Daerah</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('nama_daerah')? "is-invalid": ""}}" id="nama_daerah" name="nama_daerah"  value=" {{ old('nama_daerah') ? old('nama_daerah') : $streets['nama_daerah'] }} ">
                        <div class="invalid-feedback">
                          {{$errors->first('nama_daerah')}}
                        </div>
                    </div>
                </div>

              <br>

                <div class="row">
                    <div class="col-md-3 offset-md-5 offset-md-4">
                        <button class="btn btn-outline-primary" type="submit">Simpan</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection