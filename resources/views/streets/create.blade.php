@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
  <div class="card">
      <div class="card-header bg-success text-light">
          Create New Street
      </div>
      <div class="card-body">
        <form class="form-group" action="{{ route('streets.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
          <div class="row">
              <div class="col-md-2">
                  <label for="nama_jalan">Nama Jalan</label>
              </div>
              <div class="col-md-9">
                  <input type="text" class="form-control {{$errors->first('nama_jalan')? "is-invalid": ""}}" value=" {{ old('nama_jalan') }} " id="nama_jalan" name="nama_jalan">
                  <div class="invalid-feedback">
                    {{$errors->first('nama_jalan')}}
                  </div>
              </div>

          </div>
        <br>
          <div class="row">

              <div class="col-md-2">
                  <label for="nama_daerah">Nama Daerah</label>
              </div>
              <div class="col-md-9">
                  <input type="text" class="form-control {{$errors->first('nama_daerah')? "is-invalid": ""}}" value=" {{ old('nama_daerah') }} " id="nama_daerah" name="nama_daerah">
                  <div class="invalid-feedback">
                    {{$errors->first('nama_daerah')}}
                  </div>
              </div>
          </div>
        <br>
          <div class="row">
              <div class="col-md-3 offset-md-5 offset-md-4">
                  <button class="btn btn-outline-success" type="submit">Simpan</button>
              </div>
          </div>
        </form>
      </div>
  </div>
</div>

@endsection
