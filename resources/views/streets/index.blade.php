@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
    <div class="card">
        <div class="card-header">
            Street
        </div>

        <div class="card-body">
            <a href="{{ route('streets.create') }}" class="btn btn-success"><span data-feather="plus-square"></span> Create new</a>
            <br><br>

                <div class="tabel-responsive">

                    <table class="table table-striped">

                        <thead>
                          <tr class="table-success">
                              <th>ID</th>
                              <th>Nama Jalan</th>
                              <th>Nama Daerah</th>
                              <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>
                            @foreach($streets as $street)
                                <tr>
                                    <td> {{$street['id']}} </td>
                                    <td> {{$street['nama_jalan']}} </td>
                                    <td> {{$street['nama_daerah']}} </td>
                                    <td>
                                        <a href="{{ route('streets.show', ['id'=>$street['id']]) }}" class="btn-sm btn-success">Detail</a>
                                        <a href="{{ route('streets.edit', ['id'=>$street['id']]) }}" class="btn-sm btn-primary">Edit</a>

                                        <form action="{{ route('streets.destroy', ['id'=>$street['id']]) }}" method="POST" onsubmit="return confirm('Delete Permanen?')" class="d-inline">
                                        @csrf
                                        @method('DELETE')

                                          <button type="submit" class="btn-xs btn-danger" name="submit" value="Delete" style="border: none; border-radius: 3px;">
                                            Delete
                                          </button>

                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>

                </div>

        </div>
    </div>
</div>

@endsection
