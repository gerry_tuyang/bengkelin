<!DOCTYPE html>
<html>
<head>
	<title>Contact</title>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
	<style type="text/css">
		body{
			margin: 0;
			padding: 0;
			display: flex;
			justify-content: center;
			align-items: center;
			min-height: 100vh;
		}
		.navbar{
			margin-bottom: 630px;
			margin-right: 80px;
		}
		.home > a{
			font-family: arial;
			font-size: 18px;
			font-weight: 300;
			color: #000;
			text-decoration: none;
		}
		.container{
			width: 800px;
			margin: 0 auto;
			margin-right: 110px;
			display: flex;
			justify-content: space-between;
		}
		.container .box{
			text-align: center;

		}
		.container .box .icon .fa{
			font-size: 80px;
			cursor: pointer;
		}
		.container .box .icon h3,
		.container .box .icon h4{
			font-family: arial;
			position: relative;
			overflow: hidden;
			font-weight: 400;
			margin: 0;
			padding: 2px 5px;
			font-size: 24px;
			transition-delay: 0.3333s;
			color: transparent;
		}
		.container .box .fa:hover ~ h3,
		.container .box .fa:hover ~ h4{
			color: #000;
		}
		.container .box .icon h4{
			font-weight: 600;
			margin: 5px 0;
			font-style: 30px;
		}
		.container .box .icon h3:before,
		.container .box .icon h4:before{
			content: '';
			position: absolute;
			top: 0;
			left: 100%;
			width: 100%;
			height: 100%;
			background: #00bcd4;
			transition: 1s;
		}
		.container .box .icon h4:before{
			left: initial;
			right: 100%;
			background: #9c27b0;
		}
		.container .box .fa:hover ~ h3:before{
			left: -100%;
		}
		.container .box .fa:hover ~ h4:before{
			left: initial;
			right: -100%;
		}
	</style>

</head>
<body>

	<div class="container">
		

		<div class="box">
			<div class="icon">
				<i class="fa fa-instagram" aria-hidden="true"></i>
				<h4>Instagram</h4>
				<h3>@Bengkelin</h3>
			</div>
		</div>
		<div class="box">
			<div class="icon">
				<i class="fa fa-phone" aria-hidden="true"></i>
				<h4>Mobile</h4>
				<h3>+62852-5007-3839</h3>
			</div>
		</div>
		<div class="box">
			<div class="icon">
				<i class="fa fa-envelope" aria-hidden="true"></i>
				<h4>Email</h4>
				<h3>bengkelin@gmail.com</h3>
			</div>
		</div>
	</div>

	<div class="navbar">
        @if (Route::has('login'))
            <div class="home">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>
                    <a href="{{ route('register') }}">Register</a>
                @endauth
            </div>
        @endif
	</div>

</body>
</html>
