@extends('layouts.app')

@section('title', 'Home')

@section('content')

<div class="container">
    @foreach($bengkel as $b)
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{$b['nama_bengkel']}}
                    </div>
                    <div class="card-body">
                        <b>{{$b['jenis_bengkel']}}</b>
                        <br>
                            {{$b->street->nama_jalan}}, {{$b->street->nama_daerah}}
                        <hr>
                            <a href="">Customer</a>
                    </div>
                </div>
            </div> 
        </div>
        <br>
    @endforeach
</div>

@endsection
