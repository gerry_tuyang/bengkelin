@extends('layouts.app')

@section('title', 'Home')

@section('content')

<div class="container">
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">
                    {{$users['name']}}
                </div>
                <div class="card-body">
                    E-mail <input type="text" value="{{$users['email']}}" class="form-control" readonly><br>
                    Phone <input type="text" value="{{$users['phone']}}" class="form-control" readonly><br>
                    Alamat <input type="text" value="{{$users['address']}}" class="form-control" readonly><br>
                    <hr>
                    <a href="{{ route('home.edit', ['id'=>Auth::User()->id]) }}">Edit Profile</a>
                </div>

            </div>
        </div>
    </div>

</div>

@endsection
