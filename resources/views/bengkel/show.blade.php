@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
	<div class="card">
		<div class="card-header bg-info text-light">
			Detail Bengkel
		</div>

		<div class="card-body">

			<div class="row">
				<div class="col-md-2">
					<label for="nama_bengkel">Nama Bengkel</label>
				</div>
				<div class="col-md-9">
					<input type="text" readonly="" value="{{$bengkel['nama_bengkel']}}" class="form-control">
				</div>
			</div>

			<br>

			<div class="row">
				<div class="col-md-2">
					<label for="jenis_bengkel">Jenis Bengkel</label>
				</div>
				<div  class="col-md-9">
					<input type="text" value="{{$bengkel['jenis_bengkel']}}" class="form-control" readonly="">
				</div>
			</div>

			<br>

			<div class="row">
				<div class="col-md-2">
					<label for="no_usaha">No Usaha</label>
				</div>
				<div class="col-md-9">
					<input type="text" value=" {{$bengkel['no_usaha']}} " class="form-control" readonly="">
				</div>
			</div>

			<br>

			<div class="row">
				<div class="col-md-2">
					<label for="street_id">Alamat</label>
				</div>
				<div class="col-md-9">
					<input type="text" value=" {{$bengkel->street->nama_jalan}} " class="form-control" readonly="">
				</div>
			</div>

		</div>

	</div>
</div>

@endsection