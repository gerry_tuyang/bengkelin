@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
    <div class="card">
        <div class="card-header">
            Bengkel
        </div>

        <div class="card-body">
            <a href="{{ route('bengkel.create') }}" class="btn btn-success"><span data-feather="plus-square"></span> Create new</a>
            <br><br>

            <div class="tabel-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr class="table-success">
                            <th>ID</th>
                            <th>Nama Bengkel</th>
                            <th>Jenis Bengkel</th>
                            <th>Alamat</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($bengkel as $beng)
                            <tr>
                                <td> {{$beng['id']}} </td>
                                <td> {{$beng['nama_bengkel']}} </td>
                                <td> {{$beng['jenis_bengkel']}} </td>
                                <td> {{$beng->street->nama_jalan}} </td>
                                <td>
                                    <a href="{{ route('bengkel.show', ['id'=>$beng['id']]) }}" class="btn-sm btn-success">Detail</a>
                                    <a href="{{ route('bengkel.edit', ['id'=>$beng['id']]) }}" class="btn-sm btn-primary">Edit</a>
                                    
                                    <form action="{{ route('bengkel.destroy', ['id'=>$beng['id']]) }}" method="POST" onsubmit="return confirm('Delete Permanen?')" class="d-inline">
                                      @csrf
                                      @method('DELETE')

                                      <button type="submit" class="btn-xs btn-danger" name="submit" value="Delete" style="border: none; border-radius: 3px;">
                                          Delete
                                      </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
