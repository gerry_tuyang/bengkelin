@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
    <div class="card">
        <div class="card-header bg-info text-light">
            Edit Bengkel
        </div>

        <div class="card-body">
            <form class="form-group" action="{{ route('bengkel.update', $bengkel['id']) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

                <div class="row">
                    <div class="col-md-2">
                        <label for="nama_bengkel">Nama Bengkel</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('nama_bengkel')? "is-invalid": ""}}" id="nama_bengkel" name="nama_bengkel" value=" {{ old('nama_bengkel') ? old('nama_bengkel') : $bengkel['nama_bengkel'] }} ">
                        <div class="invalid-feedback">
                          {{$errors->first('nama_bengkel')}}
                        </div>
                    </div>
                </div>

            <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="jenis_bengkel">Jenis Bengkel</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('jenis_bengkel')? "is-invalid": ""}}" id="jenis_bengkel" name="jenis_bengkel"  value=" {{ old('jenis_bengkel') ? old('jenis_bengkel') : $bengkel['jenis_bengkel'] }} ">
                        <div class="invalid-feedback">
                          {{$errors->first('jenis_bengkel')}}
                        </div>
                    </div>
                </div>

            <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="no_usaha">No Usaha</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('no_usaha')? "is-invalid": ""}}" id="no_usaha" name="no_usaha" value=" {{old('no_usaha') ? old('no_usaha') : $bengkel['no_usaha'] }} ">
                        <div  class="invalid-feedback">
                            {{$errors->first('no_usaha')}}
                        </div>
                    </div>
                </div>

            <br>

                <div  class="row">
                    <div class="col-md-2">
                        <label for="street_id">Alamat</label>
                    </div>
                    <div class="col-md-9">
                        <select name="street_id" id="street_id" class="form-control {{$errors->first('street_id')? "is-invalid": ""}} ">
                            <option value="">Alamat</option>
                                @foreach($streets as $s)
                                    <option value=" {{$s->id}} " {{$bengkel->street_id==$s->id ? "selected": ''}}>
                                        {{$s->nama_jalan}}
                                    </option>
                                @endforeach
                        </select>
                        <div class="invalid-feedback">
                            {{$errors->first('street_id')}}
                        </div>
                    </div>
                </div>

            <br>

                <div class="row">
                    <div class="col-md-3 offset-md-5 offset-md-4">
                        <button class="btn btn-outline-primary" type="submit">Simpan</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection