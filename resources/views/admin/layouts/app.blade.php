<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('admin.layouts._head')

<body>
    
    <div id="app">
        
        @include('admin.layouts._topNav')

        <main class="py-4">
        	<div class="container">

			    <div class="row">

			        <div class="col-md-2">
			            <div class="card">
			                <div class="card-header">
			                    <a href="{{ route('admin.index') }}" class="text-dark">Admin</a>
			                </div>
			                <div class="card-body">
			                    <ul class="nav flex-column">
			                        <li class="nav-item">
			                            <a href="{{ route('users.index') }}" class="nav-link">User</a>
			                            <a href="{{ route('bengkel.index') }}" class="nav-link">Bengkel</a>
			                            <a href="{{ route('streets.index') }}" class="nav-link">Street</a>
			                        </li>
			                    </ul>
			                </div>
			            </div>
			        </div>

			        @yield('content')

			    </div>

			</div>
            
        </main>
    </div>
@include('admin.layouts._script')
</body>
</html>
