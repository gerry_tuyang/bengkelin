<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #333;
                color: #fff;
                font-family: Agency FB;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #fff;
                padding: 0 35px;
                font-size: 22px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
            }

            .m-b-md {
                margin-bottom: 30px;
                margin-right: 30px;
                margin-left: 30px;
            }
            .box{
                position: relative;
                width: 330px;
                height: 330px;
                background: linear-gradient(#158e83, #158e83);
                border-radius: 50%;
                border: 10px solid #fff;
            }
            .motor{
                position: absolute;
                top: 35px;
                left: 65px;
                display: flex;
                justify-content: center;
                align-items: center;
                width: 210px;
                height: 210px;
                animation: animate-motor 1s linear infinite;
            }
            @keyframes animate-motor{
                0%{
                    transform: translate(1px, 8px);
                }
                50%{
                    transform: translate(-1px, -8px);
                    transform: rotate(-8deg);
                }
                100%{
                    transform: translate(1px, 8px);
                }
            }
        </style>
</head>
<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>
                    <a href="{{ route('register') }}">Register</a>
                @endauth
            </div>
        @endif

        <div class="content">
            <div class="title m-b-md">
                <hr>
                Bengkelin
                <hr>
            </div>
        </div>
        
        <div class="box">
            <div class="motor">
                <img src="{{ asset('image/scooter.png') }}" alt="Scooter">
            </div>
        </div>

    </div>

</body>
</html>
