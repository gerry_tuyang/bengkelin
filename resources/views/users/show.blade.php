@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
	<div class="card">

		<div class="card-header bg-primary text-light">
			Detail User
		</div>

		<div class="card-body">

			<div class="row">
				<div class="col-md-2">
					<label for="name">Nama</label>
				</div>
				<div class="col-md-9">
					<input type="text" value="{{$users['name']}}" class="form-control" readonly="">
				</div>
			</div>

		<br>

			<div class="row">
				<div class="col-md-2">
					<label for="email">E-mail</label>
				</div>
				<div  class="col-md-9">
					<input type="text" value="{{$users['email']}}" class="form-control" readonly="">
				</div>
			</div>

		<br>

			<div class="row">
				<div class="col-md-2">
					<label for="phone">Phone</label>
				</div>
				<div class="col-md-9">
					<input type="text" value=" {{$users['phone']}} " class="form-control" readonly="">
				</div>
			</div>

		<br>

			<div class="row">
				<div class="col-md-2">
					<label for="address">Alamat</label>
				</div>
				<div class="col-md-9">
					<input type="text" value=" {{$users['address']}} " class="form-control" readonly="">
				</div>
			</div>

		</div>
	</div>
</div>

@endsection