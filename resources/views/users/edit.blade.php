@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
    <div class="card">
        <div class="card-header bg-primary text-light">
            Edit User
        </div>

        <div class="card-body">
            <form class="form-group" action="{{ route('users.update', $users['id']) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

                <div class="row">
                    <div class="col-md-2">
                        <label for="name">Nama</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('name')? "is-invalid": ""}}" id="name" name="name" value=" {{ old('name') ? old('name') : $users['name'] }} ">
                        <div class="invalid-feedback">
                          {{$errors->first('name')}}
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="email">E-mail</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('email')? "is-invalid": ""}}" id="email" name="email"  value=" {{ old('email') ? old('email') : $users['email'] }} ">
                        <div class="invalid-feedback">
                          {{$errors->first('email')}}
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="phone">Phone</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('phone')? "is-invalid": ""}}" id="phone" name="phone"  value=" {{ old('phone') ? old('phone') : $users['phone'] }} ">
                        <div class="invalid-feedback">
                          {{$errors->first('phone')}}
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="address">Alamat</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('address')? "is-invalid": ""}}" id="address" name="address"  value=" {{ old('address') ? old('address') : $users['address'] }} ">
                        <div class="invalid-feedback">
                          {{$errors->first('address')}}
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="password">Password</label>
                    </div>
                    <div class="col-md-9">
                        <input type="password" class="form-control {{$errors->first('password')? "is-invalid": ""}}" id="password" name="password" required>
                        <div class="invalid-feedback">
                            {{$errors->first('password')}}
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="password-confirm">Confirm Password</label>
                    </div>
                    <div class="col-md-9">
                        <input type="password" id="password-confirm" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-3 offset-md-5 offset-md-4">
                        <button class="btn btn-outline-primary" type="submit">Simpan</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection