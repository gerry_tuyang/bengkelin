@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
    <div class="card">
          <div class="card-header">
              User
          </div>

          <div class="card-body">
              {{-- <a href="{{ route('users.create') }}" class="btn btn-success"><span data-feather="plus-square"></span> Create new</a> --}}

                <div class="tabel-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr class="table-success">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td> {{$user['id']}} </td>
                                    <td> {{$user['name']}} </td>
                                    <td> {{$user['email']}} </td>
                                    <td> {{$user['phone']}} </td>
                                    <td>
                                        <a href="{{ route('users.show', ['id'=>$user['id']]) }}" class="btn-sm btn-success">Detail</a>
                                        <a href="{{ route('users.edit', ['id'=>$user['id']]) }}" class="btn-sm btn-primary">Edit</a>
                                        
                                        <form action="{{route('users.destroy', ['id'=>$user['id']])}}" onsubmit="return confirm('Delete Permanen?')" class="d-inline" method="POST">
                                          @csrf
                                          @method('DELETE')
                                              <button type="submit" class="btn-xs btn-danger" style="border: none;border-radius: 3px;" name="submit" value="Delete">
                                                  Delete
                                              </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

          </div>
    </div>
</div>

@endsection
