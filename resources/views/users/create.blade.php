@extends('admin.layouts.app')

@section('title', 'Admin')

@section('content')

<div class="col-md-10">
    <div class="card">
        <div class="card-header bg-success text-light">
            Create New User
        </div>
        <div class="card-body">
            <form class="form-group" action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
                <div class="row">
                    <div class="col-md-2">
                        <label for="name">Nama</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('name')? "is-invalid": ""}}" value=" {{ old('name') }} " id="name" name="name">
                        <div class="invalid-feedback">
                          {{$errors->first('name')}}
                        </div>
                    </div>
                </div>

              <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="email">E-mail</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('email')? "is-invalid": ""}}" value=" {{ old('email') }} " id="email" name="email">
                        <div class="invalid-feedback">
                          {{$errors->first('email')}}
                        </div>
                    </div>
                </div>

              <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="phone">Phone</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('phone')? "is-invalid": ""}}" value=" {{ old('phone') }} " id="phone" name="phone">
                        <div class="invalid-feedback">
                          {{$errors->first('phone')}}
                        </div>
                    </div>
                </div>

              <br>

                <div class="row">
                    <div class="col-md-2">
                        <label for="address">Alamat</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control {{$errors->first('address')? "is-invalid": ""}}" value=" {{ old('address') }} " id="address" name="address">
                        <div class="invalid-feedback">
                          {{$errors->first('address')}}
                        </div>
                    </div>
                </div>

              <br>

                <div class="row">
                    <div class="col-md-3 offset-md-5 offset-md-4">
                        <button class="btn btn-outline-primary" type="submit">Simpan</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection
