<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts._head')

<body>
    
    <div id="app">
        
        @include('layouts._topNav')

        <main class="py-4 bg-secondary" style="min-height: 640px;">
            @yield('content')
        </main>
    </div>

@include('layouts._script')
</body>
</html>
